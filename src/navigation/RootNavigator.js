import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerNavigator from './DrawerNavigator';
import LowerTabs from './LowerTabNavigator';
import JournalEntries from '../views/menuViews/journalEntries';
import Calendar from '../views/menuViews/calendar';
import ChooseTrackingQuestions from '../views/menuViews/chooseTrackingQuestions';
import Profile from '../views/menuViews/profile';
import { FontAwesome, FontAwesome5, MaterialCommunityIcons } from '@expo/vector-icons';
import FlashMessage from 'react-native-flash-message';
import Login from '../views/login';


const Drawer = createDrawerNavigator();
function RootNavigator() {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerContent={DrawerNavigator}
        initialRouteName="Login"
        drawerContentOptions={{
          activeTintColor: '#548ff7',
          activeBackgroundColor: 'transparent',
          inactiveTintColor: '#ffffff',
          inactiveBackgroundColor: 'transparent',
          backgroundColor: '#43484d',
          itemStyle: {
            margin: 0,
            padding: 0,
          },
          labelStyle: {
            fontSize: 15,
            margin: 0,
            padding: 0,
          },
        }}
      >


        {/*Example Components*/}
        {/*<Drawer.Screen name="Avatars" component={Avatars} />*/}
        {/*<Drawer.Screen name="Buttons" component={Buttons} />*/}
        {/*<Drawer.Screen name="Inputs" component={Inputs} />*/}
        {/*<Drawer.Screen name="Lists" component={Lists} />*/}
        {/*<Drawer.Screen name="Lists2" component={Lists2} />*/}
        {/*<Drawer.Screen name="Cards" component={Cards} />*/}
        {/*<Drawer.Screen name="Tiles" component={Tiles} />*/}
        {/*<Drawer.Screen name="Pricing" component={Pricing} />*/}
        {/*<Drawer.Screen name="Ratings" component={Ratings} />*/}
        {/*<Drawer.Screen name="Settings" component={Settings} />*/}
        {/*<Drawer.Screen name="Fonts" component={Fonts} />*/}

        {/*onPress: () => navigation.navigate('Home', { screen: 'Home' }),*/}



        <Drawer.Screen name="Choose Questions" component={ChooseTrackingQuestions} options={{
          drawerIcon: ({ color }) => <MaterialCommunityIcons name="magnify" size={30} color={color}/>,
        }}/>

        <Drawer.Screen name="Journal Entries" component={JournalEntries} options={{
          drawerIcon: ({ color }) => (<FontAwesome5 name="pencil-alt" size={28} color={color}/>),
        }}/>
        <Drawer.Screen name="View Calendar" component={Calendar} options={{
          drawerIcon: ({ color }) => (<FontAwesome name="calendar" size={28} color={color}/>),
        }}/>
        <Drawer.Screen name="My Profile" component={Profile} options={{
          drawerIcon: ({ color }) => (<FontAwesome name="user-circle" size={28} color={color}/>),
        }}/>

        <Drawer.Screen name="Login" component={Login}/>

        <Drawer.Screen name="Home" component={LowerTabs}
          // options={{ drawerIcon: ({ color }) => <MaterialCommunityIcons name="home" size={30} color={color}/>}}
        />

        {/*<Drawer.Screen name="View Trends" component={Trends} options={{ drawerIcon: ({ color }) => (<Entypo name="line-graph" size={24} color={color} />)}} />*/}
        {/*<Drawer.Screen name="Push Notifications" component={pushNotificationLogic} />*/}
        {/*<Drawer.Screen name="TrackingQ" component={TrackingQ} />*/}
      </Drawer.Navigator>

      {/*This vv is necessary for all flash messages*/}
      <FlashMessage position="top"/>
    </NavigationContainer>
  );
}

export default RootNavigator;
