import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Journal from '../views/homeViews/journal';
import HomeScreen from '../views/homeViews/homeScreen';
import TrackingQ from '../views/homeViews/answerTrackingQuestions';
import Resources from '../views/homeViews/resources';
import Trends from '../views/menuViews/trends';
import { Entypo, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

export default function lowerTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        style: { height: '10%' },
        inactiveTintColor: 'black',
        keyboardHidesTabBar: true,
        tabStyle: {
          paddingBottom: 5,
          paddingTop: 8
        },
        labelStyle: {
          fontSize: 12,
        },
      }}

    >
      <Tab.Screen name="Questions" component={TrackingQ} options={{
        tabBarIcon: ({ color }) => <MaterialCommunityIcons name="magnify" size={24} color={color}/> }}/>
      <Tab.Screen name="Resources" component={Resources} options={{
        tabBarIcon: ({ color }) => <MaterialCommunityIcons name="clipboard-text-outline" size={24} color={color}/> }}/>
      <Tab.Screen name="Home" component={HomeScreen} options={{
        tabBarIcon: ({ color }) => <MaterialCommunityIcons name="home" size={30} color={color}/> }}/>
      <Tab.Screen name="Journal" component={Journal} options={{
        tabBarIcon: ({ color }) => <FontAwesome name="pencil-square-o" size={24} color={color}/> }}/>
      <Tab.Screen name="Trends" component={Trends} options={{
        tabBarIcon: ({ color }) => (<Entypo name="line-graph" size={24} color={color}/>) }}/>
    </Tab.Navigator>
  );
}
