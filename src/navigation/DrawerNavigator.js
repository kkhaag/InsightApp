import React from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import { DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const CustomContentComponent = (props) => (

  <SafeAreaView
    style={{ flex: 1, height: '100%', backgroundColor: '#43484d' }}
    forceInset={{ top: 'always', horizontal: 'never' }}
  >
    <View
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text style={{
        justifyContent: 'center',
        alignItems: 'center',
        padding: 25,
        fontSize: 40,
        color: 'white',
      }}>INSIGHT</Text>

    </View>
    <View style={{ marginLeft: 10 }}>
      <DrawerItem
        label="Home"
        labelStyle={{ color:"#ffffff", fontSize: 15 }}
        icon={() => <MaterialCommunityIcons name="home" size={30} color={"#ffffff"}/>}
        onPress={() => props.navigation.navigate('Home', { screen: 'Home' })}
      />

      <DrawerItemList {...props} />

    </View>
  </SafeAreaView>
);

function CustomDrawerContent(props) {
  const { state, ...rest } = props;
  const newState = { ...state}  //copy from state before applying any filter. do not change original state
  newState.routes = newState.routes.filter(item => item.name !== 'Login') //replace "Login' with your route name
  newState.routes = newState.routes.filter(item => item.name !== 'Home')

  return (
    <DrawerContentScrollView {...props}>
      <CustomContentComponent state={newState} {...rest}  />
    </DrawerContentScrollView>
  );
}

export default CustomDrawerContent;
