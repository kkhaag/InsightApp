import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import * as LocalAuthentication from 'expo-local-authentication';
import 'intl';
import { Platform } from 'react-native';
import 'intl/locale-data/jsonp/en';
import { showMessage } from 'react-native-flash-message';



if (Platform.OS === 'android') {
  if (Intl.__disableRegExpRestore === 'function') {
    Intl.__disableRegExpRestore();
  }
}


export const ShowMessage = async (color, position, msg, duration = 1850) => {
  try {
    showMessage({
      message: msg,
      type: color,
      position: position,
      duration: duration,
    });
  } catch (e) {
    console.error('Failed to show alerting message');
  }
};


// export const sendPushNotification = async (title, body, navigate, badge = 1) => {
//   await Notifications.scheduleNotificationAsync({
//     content: {
//       title: title,
//       body: body,
//       data: { 'navigate': navigate }, //Resources, Questions
//       sound: 'default',
//       badge: badge,
//     },
//     trigger: {
//       seconds: 1,
//       repeats: false,
//     },
//   });
// };


export const Load = async (STORAGE_KEY) => {
  try {
    const data = await AsyncStorage.getItem(STORAGE_KEY);
    // console.log('Inside LOAD', data);
    if (data !== null) {
      let parsedData = JSON.parse(data);
      // console.log("GetItem parsedData", parsedData)
      return parsedData;
    }
  } catch (e) {
    console.error('Failed to getItem from Async Storage');
  }
};

export const Save = async (data, STORAGE_KEY) => {
  try {
    // console.log('Inside SAVE', data);

    // console.log("PRE-STRINGIFIED", data)
    data = JSON.stringify(data);
    // console.log("STRINGIFIED", data)
    await AsyncStorage.setItem(STORAGE_KEY, data);
    // console.log("Set Item!")
  } catch (e) {
    console.error('Failed to setItem in Async Storage');
  }
};

export const Delete = async (STORAGE_KEY) => {
  try {
    await AsyncStorage.removeItem(STORAGE_KEY);
    console.log('Removed Item:', STORAGE_KEY);
  } catch (e) {
    console.error('Failed to removeItem in Async Storage');
  }
};


export const GetAllKeys = async () => {
  try {
    const keys = await AsyncStorage.getAllKeys();
    const result = await AsyncStorage.multiGet(keys);
    // console.log ("KEYS", keys)
    // console.log ("RESULT", result)
    // return result.map(req => JSON.parse(req)).forEach(console.log);
  } catch (error) {
    console.error('Error getting all keys', error);
  }
};

export const getCurrentTime = async () => {
  // let options = {
  //   day: 'numeric', month: 'numeric',
  //   hour12: false,
  // };
  // let date = Intl.DateTimeFormat('default', options).format(date);

  let date = moment(new Date()).format('YYYY-MM-DD');

  let options = {
    hour: 'numeric', minute: 'numeric', second: 'numeric',
    hour12: false,
  };
  let time = Intl.DateTimeFormat('default', options).format(time);

  return { date: date, time: time };

};


export const CheckDate = (rateQuestion) => {
  // console.log('INSIDE CheckDate', rateQuestion.nextAnswerDate);
  if (rateQuestion.nextAnswerDate === '') {
    return true;
  } else {
    return moment(rateQuestion.nextAnswerDate).isBefore(moment());

  }
};

async function CheckDateAndChecked(Q) {

  // let checkedDate = await CheckDate(Q);
  // console.log('BOTH:', checkedDate, Q.isChecked, Q.questionText);
  //
  // if (Q.isChecked && checkedDate) {
  //   console.log('BOTH TRUE!!!!');
  //   return true;
  // }
  // return false;
}

export const haveQuestions = async (answer) => {
  // return answer.some(CheckDateAndChecked);

  for (let rateQuestion of answer) {
    // console.log("PRE Checked Date", rateQuestion)
    let checkedDate = await CheckDate(rateQuestion);
    // console.log("checkedDate + isChecked", checkedDate, rateQuestion.isChecked)
    if (rateQuestion.isChecked && checkedDate) {
      return true;
    }
  }
  return false;
};


// export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");

// export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const isSignedIn = () => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('email')
      .then(res => {
        if (res !== null) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
      .catch(err => reject(err));
  });
};

export const getLocalAuthToken = async () => {
  const hasHardware = await LocalAuthentication.hasHardwareAsync();
  let isSupported = await LocalAuthentication.supportedAuthenticationTypesAsync('com.samsung.android.bio.face');
  let isEnrolled = await LocalAuthentication.isEnrolledAsync();
  console.log('hasHardware?', hasHardware);
  console.log('isSupported?', isSupported);
  console.log('isEnrolled?', isEnrolled);

  let deviceFallback = true;
  if (Platform.OS === 'android' && isSupported.length < 1 && isEnrolled) {
    // deviceFallback = false;
    let makeShiftObj = {
      "error": "Device is 'Enrolled' but there are 0 supported authentication methods. This is possibly an inexpensive device",
      "success": false,
      "warning": "Enrolled but no authentication methods supported",
    }
    return makeShiftObj
  }
  console.log("RIGHT HERE")
  let result = await LocalAuthentication.authenticateAsync({
    promptMessage: 'Authenticate',
    fallbackLabel: '',
    cancelLabel: 'Cancel',
    disableDeviceFallback: deviceFallback,
  });
  console.log('result', result);
  return result;
};
