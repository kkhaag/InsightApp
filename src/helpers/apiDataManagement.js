import React from 'react';
import { Load, Save, ShowMessage } from './helperFunctions';

// let email;


export const uploadPasswordOrExpoToken = async (dataToUpdate, email) => {
  dataToUpdate = JSON.stringify(dataToUpdate);
  let response = await fetch(
    `https://insightsserverapi.herokuapp.com/api/patients/${email}`,
    {
      method: 'PUT',
      headers: { 'Content-type': 'application/json' },
      body: dataToUpdate,
    });
  if (!response.ok || !response) {
    console.log("Upload Pass or Push Token Issue: ", JSON.stringify(response))
    alert('There was an issue uploading the push token, please check your internet connection and try again');
  }
}


export const getUserAndPassword = async (email) => {
  try {
    let response = await fetch(`https://insightsserverapi.herokuapp.com/api/patientByEmail/${email}`);
    if (!response.ok) {
      return response.ok;
    }
    let responseJson = await response.json();
    return responseJson.password;
  } catch (error) {
    console.error(error);
  }
};


export const fetchAllData = async () => {

  // await Save('tschmoek@gmail.com', 'email');
  let email = await Load('email');

  try {
    let response = await fetch(`https://insightsserverapi.herokuapp.com/api/patientByEmail/${email}`);
    if (!response.ok || !response) {
      alert('There was an issue communicating with the server, please check your internet connection and credentials');
      return response.ok;
    }

    let responseJson = await response.json();

    await saveJournals(responseJson.journal);
    await saveClinic(responseJson.clinic);
    await saveDoctor(responseJson.doctor);
    await saveSurveys(responseJson.surveys[0].questions);
    await saveResources(responseJson.resources);
    await saveUser(responseJson);
    return true;

  } catch (error) {
    console.error(error);
    await ShowMessage('danger', 'top', 'Failed to retrieve app data')
  }
};

const saveJournals = async (journals) => {
  // console.log('Journals', journals);
  await Save(journals, 'journals');
};

const saveClinic = async (clinics) => {
  // console.log('Clinics', clinics);
  await Save(clinics, 'clinics');
};

const saveDoctor = async (doctors) => {
  // console.log('Doctors', doctors);
  await Save(doctors, 'doctors');
};

const saveResources = async (resources) => {
  // console.log('Resources', resources);
  await Save(resources, 'resources');
};

const saveSurveys = async (surveys) => {
  // await Delete('surveys')
  // console.log('Surveys', surveys);
  await Save(surveys, 'surveys');
};

const saveUser = async (userData) => {
  let userInfo = {
    userId: userData._id,
    firstName: userData.first_name,
    lastName: userData.last_name,
    phone: userData.phone,
    dateOfBirth: userData.date_of_birth,
    password: userData.password,
    email: userData.email,
    doctorId: userData.doctor_ID,
  };
  // console.log('UserData', userInfo);
  await Save(userInfo, 'user');
};

const saveEmail = async (userEmail) => {
  // console.log('UserEmail', userEmail);
  await Save(userEmail, 'email');
};


export const uploadSurveys = async (survey) => {
  let email = await Load('email');
  let response;
  // console.log('survey', survey);
  try {
    response = await fetch(
      `https://insightsserverapi.herokuapp.com/api/surveys/${email}`,
      {
        method: 'PUT',
        headers: { 'Content-type': 'application/json' },
        body: survey,
      });
  }
  catch(err) {
    await ShowMessage('danger', 'top', 'Failed to Save')
  }

  if (!response.ok || !response) {
    alert('There was an issue connecting to the server, please check your internet connection and try again');
    return false;
  }

  let allQuestions = await response.json();
  await Save(allQuestions, 'surveys');
  return true;
};

export const uploadJournal = async (journal) => {
  let email = await Load('email');
  let response;
  try {
    response = await fetch(
      `https://insightsserverapi.herokuapp.com/api/patientJournal/${email}`,
      {
        method: 'PUT',
        headers: { 'Content-type': 'application/json' },
        body: journal,
      });
  }
  catch(err) {
    await ShowMessage('danger', 'top', 'Failed to Save')
  }

  if (!response.ok || !response) {
    alert('There was an issue connecting to the server, please check your internet connection and try again');
    return response;
  }

  let allJournals = await response.json();
  await Save(allJournals, 'journals');
  return response;
};
