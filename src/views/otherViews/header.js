import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Header as HeaderRNE } from 'react-native-elements';

function Header(props) {
  const navigation = useNavigation();
  let canGoBack;
  props.title === 'Home' ? canGoBack = false : canGoBack = true;

  return (
    <HeaderRNE
      rightComponent={{
        icon: 'menu',
        color: '#fff',
        onPress: navigation.openDrawer,
      }}

      leftComponent={{
        icon: canGoBack ? 'home' : null,
        color: '#fff',
        onPress: () => navigation.navigate('Home', { screen: 'Home' }),
      }}
      centerComponent={{ text: props.title, style: styles.heading }}
    />
  );
}


function SubHeader({ icon, iconType, title }) {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.heading}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 40,
    backgroundColor: '#ED553B',
    marginBottom: 20,
  },
  heading: {
    color: 'white',
    marginTop: 10,
    fontSize: 22,
    fontWeight: 'bold',
  },
});

export { Header, SubHeader };
