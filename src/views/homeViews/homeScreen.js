import React, { useEffect, useRef, useState } from 'react';
import { AppState, Dimensions, KeyboardAvoidingView, Platform, Keyboard, StyleSheet, Text as TextDiv, View } from 'react-native';
import { PieChart } from 'react-native-svg-charts';
import { Circle, ClipPath, G, Image, Text } from 'react-native-svg';
import { Header } from '../otherViews/header';
import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import { fetchAllData } from '../../helpers/apiDataManagement';
import 'intl';
import 'intl/locale-data/jsonp/en';
import { getLocalAuthToken, isSignedIn } from '../../helpers/helperFunctions';

if (Platform.OS === 'android') {
  if (Intl.__disableRegExpRestore === 'function') {
    Intl.__disableRegExpRestore();
  }
}
const SCREEN_WIDTH = Dimensions.get('window').width;

const HomeScreen = ({ navigation }) => {

  const [appState, setAppState] = useState(AppState.currentState);

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const handleAppStateChange = nextAppState => {
    if (appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!');
    }
    setAppState(nextAppState);
  };

  AppState.addEventListener('change', handleAppStateChange);
  const appCurrentState = AppState.currentState;
  const previousAppState = usePrevious(appCurrentState);


  useEffect(() => {
    // console.log("--> CALLED IN HOME" )
    Keyboard.dismiss()

    if (appCurrentState === 'active' && previousAppState === 'background') {
      console.log('HELLO: ', appCurrentState, previousAppState);
      console.log('COMING TO FOREGROUND PICKED UP');

      async function loginStatus() {
        let emailFoundForSignIn = await isSignedIn();
        console.log('emailFoundForSignIn', emailFoundForSignIn);

        if (!emailFoundForSignIn) {
          navigation.navigate('Login');
          return
        }
        Keyboard.dismiss()
        let token = await getLocalAuthToken();
        console.log('getLocalAuthToken', token);

        if (emailFoundForSignIn && token.success) {
          console.log('Has email and localAuth is successful ');
          navigation.navigate('Home', { screen: 'Home' });
        }
        else {
          navigation.navigate('Login');
        }
      }
      loginStatus();
    }
  }, [appCurrentState]);

  useEffect(() => {
    console.log('SECOND USEEFFECT IN HOME');
    // Delete("email")
  }, []);


  const [state, setState] = useState(
    {
      lastApiCall: '2020-02-02',
    });

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let tryAgain = await moment(state.lastApiCall).isBefore(moment());
      let scheduleNextApiCall = moment().add(30, 'm').toDate();

      async function doStuff() {
        await setState({ ...state, lastApiCall: scheduleNextApiCall });
        // console.log("LastAPI CALL",state.lastApiCall)
        await fetchAllData();
      }

      tryAgain ? await doStuff() : null;
    });
  }, []);

  const useNav = useNavigation();
  let time = new Date().getTime();
  let date = Intl.DateTimeFormat('default', {
    year: 'numeric', month: 'short', day: 'numeric',
  }).format(time);

  const data = [
    {
      key: 1,
      amount: 1,
      category: 'Resources',
      svg: {
        fill: '#0a46ad',
        onPress: () => useNav.jumpTo('Resources'),
      },
    },
    {
      key: 2,
      amount: 1,
      category: 'Journal',
      svg: {
        fill: '#3c94ff',
        onPress: () => useNav.jumpTo('Journal'),
      },
    },
    {
      key: 4,
      amount: 1,
      category: 'Trends',
      svg: {
        fill: '#1a53ff',
        onPress: () => useNav.jumpTo('Trends'),
      },
    },
    {
      key: 3,
      amount: 1,
      category: 'Questions',
      svg: {
        fill: '#0510c9',
        onPress: () => useNav.jumpTo('Questions'),
      },
    },
  ];

  return (
    <>
      <Header title="Home"/>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={'padding'}
        enabled
        keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 84}
      >
        <View style={styles.HomeContainer}>

          <TextDiv style={{ color: 'black', fontSize: 30, textAlign: 'center' }}>{date}</TextDiv>

          <PieChart
            style={{ height: '80%' }}
            valueAccessor={({ item }) => item.amount}
            data={data}
            spacing={0}
            outerRadius={'90%'}
            innerRadius={'30%'}
          >
            <Labels/>
          </PieChart>
        </View>

      </KeyboardAvoidingView>
    </>
  );
};

const Labels = ({ slices, height, width }) => {
  return slices.map((slice, index) => {
    const { labelCentroid, pieCentroid, data } = slice;
    return (

      <React.Fragment key={index}>
        <G
          // key={index}
          x={labelCentroid[3]}
          y={labelCentroid[3]}
        >
          <ClipPath id="clip">
            <Circle r={'15.5%'}/>
          </ClipPath>

          <Image
            x={'-20%'}
            y={'-20%'}
            width={'40%'}
            height={'40%'}
            opacity="1"
            href={require('../../../assets/icons/insight_logo.png')}
            clipPath="url(#clip)"
          />
        </G>

        <G>
          <Text
            // key={index}
            x={pieCentroid[0]}
            y={pieCentroid[1]}
            fill={'white'}
            textAnchor={'middle'}
            alignmentBaseline={'middle'}
            fontSize={'18'}
            stroke={'black'}
            strokeWidth={0.2}
          >
            {data.category}
          </Text>
        </G>
      </React.Fragment>
    );
  });
};


export default HomeScreen;


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(227,226,226,.3)',
    height: '100%',
    justifyContent: 'center',
  },
  HomeContainer: {
    backgroundColor: 'rgba(227,226,226,0)',
    marginBottom: '10%',
  },
  heading: {
    color: 'white',
    marginTop: 10,
    fontSize: 22,
    fontWeight: 'bold',
  },
  contentView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  triangleLeft: {
    position: 'absolute',
    left: -20,
    bottom: 0,
    width: 0,
    height: 0,
    borderRightWidth: 20,
    borderRightColor: 'white',
    borderBottomWidth: 25,
    borderBottomColor: 'transparent',
    borderTopWidth: 25,
    borderTopColor: 'transparent',
  },
  triangleRight: {
    position: 'absolute',
    right: -20,
    top: 0,
    width: 0,
    height: 0,
    borderLeftWidth: 20,
    borderLeftColor: 'white',
    borderBottomWidth: 25,
    borderBottomColor: 'transparent',
    borderTopWidth: 25,
    borderTopColor: 'transparent',
  },
  inputContainerStyle: {
    marginTop: 16,
    width: '90%',
  },
  keyboardAvoidingView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

