import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Keyboard,
} from 'react-native';
import { Button } from 'react-native-elements';

import { Header } from '../otherViews/header';
import { FontAwesome5 } from '@expo/vector-icons';
import moment from 'moment';
import { uploadJournal } from '../../helpers/apiDataManagement';
import { Delete, ShowMessage } from '../../helpers/helperFunctions';
const SCREEN_WIDTH = Dimensions.get('window').width;


const Journal = ({ navigation }) => {
  const [state, setState] = useState({
    emotion: 'meh',
    journalContent: '',
    displayDate: '',
  });

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let time = await moment().format('YYYY-MM-DD');
      setState({ ...state, displayDate: time });
    });
  }, [state]);

  async function handlePress(emotion) {
    setState({ ...state, emotion: emotion });
  }

  async function handleSave() {
    Keyboard.dismiss()
    await new Promise(r => setTimeout(r, 500));

    if (state.journalContent.length < 2) {
      await ShowMessage("danger", "top", "Please share more")
      return
    }
    let status = await uploadJournal(JSON.stringify(state));

    if (!status.ok) return

    await setState({ ...state, emotion: 'meh', journalContent: '' });
    navigation.navigate('Home', { screen: 'Home' })
    await ShowMessage("success", "top", "Journal Entry Saved Successfully", 2000)
  }

  return (
    <KeyboardAvoidingView
      style={styles.keyboardAvoidingView}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 84}
    >
      <Header title="Journal"/>

      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps="handled"
      >
        <Text style={[styles.titleText, { marginTop: 30, color: '#666666', fontSize: 24 }]}>
          How do you feel?
        </Text>

        <View style={{ flexDirection: 'row', justifyContent: 'space-around', margin: 10 }}>

          <TouchableOpacity>
            <FontAwesome5 name="frown" size={65} color="black"
                          style={
                            state.emotion === 'frown'
                              ? styles.frown
                              : styles.notPress
                          }
                          onPress={() => handlePress('frown')}
            />
          </TouchableOpacity>

          <TouchableOpacity>
            <FontAwesome5 name="meh" size={65}
                          style={
                            state.emotion === 'meh'
                              ? styles.meh
                              : styles.notPress
                          }
                          onPress={() => handlePress('meh')}
            />
          </TouchableOpacity>

          <TouchableOpacity>
            <FontAwesome5 name="smile" size={65}
                          style={
                            state.emotion === 'smile'
                              ? styles.smile
                              : styles.notPress
                          }
                          onPress={() => handlePress('smile')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.textAreaContainer}>
          <TextInput
            style={styles.textArea}
            underlineColorAndroid="transparent"
            placeholder="Share your thoughts"
            placeholderTextColor="grey"
            multiline={true}
            blurOnSubmit={true}
            value={state.journalContent}
            onChangeText={(text) => setState({ ...state, journalContent: text })}
          />

        </View>

        <View style={{ alignItems: 'center', marginTop: 10 }}>

          <Button
            title="Save"
            containerStyle={{ height: 40, width: 150 }}
            titleStyle={{ color: 'white', marginHorizontal: 20 }}
            onPress={() => handleSave()}
          />
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default Journal;


const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(227,226,226,0.3)',
  },
  textAreaContainer: {
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    // height: 150,
    borderColor: 'grey',
    borderWidth: 3,
    backgroundColor: 'white',
    paddingLeft: 10,
    paddingRight: 10,
  },
  textArea: {
    justifyContent: 'center',
    height: 100,
    fontSize: 19,
  },
  keyboardAvoidingView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 5,
    fontFamily: Platform.OS === 'ios' ? 'Arial Rounded MT Bold' : null,
    color: '#27ae60',
  },
  frown: {
    color: 'red',
  },
  meh: {
    color: '#F1C40F',
  },
  smile: {
    color: 'green',
  },
  notPress: {
    color: '#000000',
  },
});

