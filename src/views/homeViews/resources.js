import React, { useEffect, useState } from 'react';
import { ActivityIndicator, Image, Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Header } from '../otherViews/header';
import { ListItem } from 'react-native-elements';
import { Load } from '../../helpers/helperFunctions';
import * as Notifications from 'expo-notifications';
import 'intl';
import 'intl/locale-data/jsonp/en';

if (Platform.OS === 'android') {
  if (Intl.__disableRegExpRestore === 'function') {
    Intl.__disableRegExpRestore();
  }
}

let time = new Date().getTime();
let date = Intl.DateTimeFormat('default', { year: 'numeric', month: 'short', day: 'numeric' }).format(time);

const Resources = ({ navigation }) => {
  const [resources, setResources] = useState();

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      await Notifications.setBadgeCountAsync(0);
      // await getDoctorResources();
      let resourceData = await Load('resources');
      setResources(resourceData);
    });
  }, []);

  let timeDate;
  return (
    resources === undefined ?
      <View style={[styles.container, styles.horizontal]}>
        <ActivityIndicator size="large" color="#0000ff"/>
      </View>
      :
      <>
        <Header title="Resources"/>
        {resources.length === 0 ?
          <Text style={[styles.container, styles.horizontal]}>
            The doctor has not yet provided any resources
          </Text>
          :

          <ScrollView>
            <View>
              {[...resources].reverse().map((resourceData, i) => (
                timeDate = new Date(resourceData.date_created).toLocaleDateString(),
                  <ListItem key={i} bottomDivider>
                    <ListItem.Content>
                      <ListItem.Title>
                        <Text>{timeDate}</Text>
                      </ListItem.Title>
                      <ListItem.Subtitle>
                        {resourceData.image_url === '' ? <Text>{'\n'}{resourceData.note}</Text> :
                          <Image
                            source={{ uri: resourceData.image_url }}
                            style={{ width: 300, height: 300, resizeMode: 'contain' }}
                          />
                        }
                        <Text>{resourceData.note}</Text>
                      </ListItem.Subtitle>
                    </ListItem.Content>
                  </ListItem>
              ))}
            </View>
          </ScrollView>
        }
      </>
  );
};

export default Resources;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});


// OLD METHOD FOR CALLING RESOURCE DATA
// async function getDoctorResources() {
//   try {
//     let email = await Load('email');
//     let response = await fetch(`https://insightsserverapi.herokuapp.com/api/patientByEmail/${email}`);
//     let responseJson = await response.json();
//     console.log('Res Json', responseJson.resources);
//     await Save(responseJson.resources, 'resources'); //Save only Resources
//   } catch (error) {
//     console.error(error);
//   }
// }
