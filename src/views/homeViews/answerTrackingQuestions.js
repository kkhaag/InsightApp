import React, { useEffect, useState, useReducer } from 'react';
import { ActivityIndicator, Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Header } from '../otherViews/header';
import Slider from '@react-native-community/slider';
import { CheckDate, getCurrentTime, haveQuestions, Load, ShowMessage } from '../../helpers/helperFunctions';
import { Button } from 'react-native-elements';
import * as Notifications from 'expo-notifications';
import { uploadSurveys } from '../../helpers/apiDataManagement';
import moment from 'moment';


const AnswerTrackingQuestions = ({ navigation }) => {
  const [answer, setAnswer] = useState(undefined);
  const [renderQuestions, setRenderQuestions] = useState(false);

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let data = await Load('surveys');
      let newData = await CheckWeeklyDate(data)
      setAnswer(data);

      let shouldRenderQ = await haveQuestions(data)
      setRenderQuestions(shouldRenderQ)
    });
  }, []);

  const CheckWeeklyDate = async (questions) => {

    for (const Q of questions) {
      if (Q.frequency === 2) {
        let duration = moment.duration(moment().diff(Q.nextAnswerDate))
        let hours = duration.asHours();
        if (hours > 24) {
          let dt = await new Date();
          let nextWeek = await new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 6);
          Q.nextAnswerDate = nextWeek;
        }
      }
    }
    return questions
  }


  const handleChange = (value, rateQuestion) => {
    answer.forEach(trackingQ => {
      if (trackingQ._id === rateQuestion._id) {
        trackingQ.ratingValue = value;
      }
    });
    setAnswer([...answer]);
  };

  const HandleSave = async (storageKey) => {
    const { date, time } = await getCurrentTime();
    let dt = await new Date();
    let ampm = await new Date();
    ampm.getHours() >= 12 ? ampm.setHours(24, 0, 0, 0) : ampm.setHours(12, 0, 0, 0);
    let tomorrow = await new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1);
    let nextWeek = await new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 7);
    let incrementDate = '';

    answer.forEach(Q => {
      if (Q.ratingValue > 0) {
        switch (Q.frequency) {
          case 0:
            incrementDate = ampm;
            // console.log('Case0', incrementDate, ampm);
            break;
          case 1:
            incrementDate = tomorrow;
            // console.log('Case1', incrementDate, tomorrow);
            break;
          case 2:
            incrementDate = nextWeek;
            // console.log('Case2', incrementDate, nextWeek);
            break;
        }
        let rating = Q.ratingValue;
        Q.nextAnswerDate = incrementDate;
        Q.responses.push({ ratingNumber: rating, displayDate: date, time: time });
        Q.ratingValue = 0; // Reset value back to 0 for next time
      }
    });

    await setAnswer([...answer]);
    let questions = { questions: answer };
    let status = await uploadSurveys(JSON.stringify(questions));
    if (!status) return;

    let shouldRenderQ = await haveQuestions(answer)
    // await setRenderQuestions(shouldRenderQ)
    if (shouldRenderQ) {
      await ShowMessage("success", "top", "Saved Successfully")
      return
    }
    navigation.navigate('Home', { screen: 'Home' })
    await ShowMessage("success", "top", "All Questions Saved Successfully")
  };

  const AnswerQuestions = (props) => {
    {
      if (props.disabled) {
        return (
          answer.map((rateQuestion, i) => {
              return (
                rateQuestion.isChecked ?
                  <View key={i} style={styles.container}>
                    <Text style={[styles.subtitleTextDisabled]}>
                      {rateQuestion.questionText}
                    </Text>
                  </View>
                  : null
              );
            },
          )
        );
      } else {
        return (
          answer.map((rateQuestion, i) => {
            return (
              rateQuestion.isChecked && CheckDate(rateQuestion) ?
                <View key={i} style={styles.container}>
                  <Text style={[styles.subtitleText]}>
                    {rateQuestion.questionText}
                  </Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Slider
                      minimumTrackTintColor={Platform.OS === 'android' ? "#007aff" : null}
                      maximumTrackTintColor={Platform.OS === 'android' ? "#007aff" : null}
                      thumbTintColor={Platform.OS === 'android' ? "#007aff" : null}
                      maximumValue={10}
                      minimumValue={0}
                      step={1}
                      value={rateQuestion.ratingValue}
                      onSlidingComplete={(value) => handleChange(value, rateQuestion)}
                      style={{ width: '90%', height: 80 }}
                    />
                    <Text style={{ fontSize: 25 }}>
                      {rateQuestion.ratingValue}
                    </Text>
                  </View>
                </View>
                : null
            );
          })
        );
      }
    }
  };


  return (
    <>
      <Header title="Tracking Questions"/>
      {answer === undefined ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <View style={styles.container}>
          <ScrollView style={styles.viewContainer}>
            { renderQuestions ? (
                <>
                  <Text style={[styles.titleText, { marginTop: 10, color: '#555555', fontSize: 22 }]}>
                    Rate each question {'\n'} (1-Lowest to 10-Highest)
                  </Text>
                  <View>
                    <AnswerQuestions disabled={false}/>
                  </View>


                  <Button
                    title="Save"
                    style={{ marginTop: 50, width: 200, alignSelf: 'center' }}
                    onPress={() => HandleSave('trackingQs')}
                  />
                </>
              ) :

              <View>
                <Text style={[styles.titleText, { marginTop: 10, color: '#555555', fontSize: 22 }]}>
                  You don't have any questions to answer right now. Please check back later
                </Text>
                <AnswerQuestions disabled={true}/>
              </View>
            }
          </ScrollView>
        </View>}
    </>
  );
};

export default AnswerTrackingQuestions;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(227,226,226,0.3)',
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  headingContainer: {
    paddingTop: 50,
  },
  titleText: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingVertical: 5,
    fontFamily: Platform.OS === 'ios' ? 'Arial Hebrew' : null,
    color: '#27ae60',
  },
  subtitleText: {
    fontSize: 18,
    margin: 10,
    fontWeight: '400',
    textAlign: 'left',
    fontFamily: Platform.OS === 'ios' ? 'Trebuchet MS' : null,
    color: '#34495e',
  },
  subtitleTextDisabled: {
    fontSize: 18,
    margin: 10,
    fontWeight: '400',
    textAlign: 'left',
    fontFamily: Platform.OS === 'ios' ? 'Trebuchet MS' : null,
    color: 'grey',
  },
  viewContainer: {
    flex: 1,
    margin: 10,
  },
});

