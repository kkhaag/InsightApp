import React from 'react';
import { ScrollView, Text } from 'react-native';
import { Header } from '../otherViews/header';


const AllVideos = () => {
  return (
    <>
      <Header title="All Videos"/>
      <ScrollView>
        <Text>Videos Here</Text>
      </ScrollView>
    </>
  );
};

export default AllVideos;
