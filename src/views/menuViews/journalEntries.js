import { ActivityIndicator, ScrollView, StyleSheet, View } from 'react-native';

import { Header } from '../otherViews/header';
import { Load } from '../../helpers/helperFunctions';
import React, { useEffect, useState } from 'react';
import { Icon, ListItem } from 'react-native-elements';
import moment from 'moment';

const JournalEntries = ({ navigation }) => {
  const [state, setState] = useState();

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let data = await Load('journals');
      setState(data);
    });
  }, []);

  return (
    <>
      <Header title="All Journals"/>
      {state === undefined ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <ScrollView>
          <View>
            {[...state].reverse().map((data, i) => (
              <ListItem key={i} bottomDivider>
                <Icon
                  name={data.emotion}
                  type="font-awesome-5"
                  color="rgba(0, 0, 0, 0.6)"
                  size={25}
                  style={{ backgroundColor: 'transparent' }}
                />
                <ListItem.Content>
                  <ListItem.Title>
                    {moment(data.displayDate).format('MMM DD')}
                  </ListItem.Title>
                  <ListItem.Subtitle>
                    {data.journalContent}
                  </ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>
            ))}
          </View>
        </ScrollView>
      }
    </>
  );
};

export default JournalEntries;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(227,226,226,0.3)',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  }
});







