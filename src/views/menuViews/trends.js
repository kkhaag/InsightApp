import { Header } from '../otherViews/header';
import * as React from 'react';
import { useEffect, useState } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, View, Text as Txt } from 'react-native';
import moment from 'moment';

import { VictoryAxis, VictoryChart, VictoryLabel, VictoryLine, VictoryScatter } from 'victory-native';
import { Load } from '../../helpers/helperFunctions';

const Trends = ({ navigation }) => {
  const [answer, setAnswer] = useState([]);

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      console.log("TRENDS Use E ")

      let data = await Load('surveys');
      await setAnswer(data);
    });
  }, []);

  const getDatesArr = (startDate, stopDate, unit) => {
    let dateArray = [];
    // console.log("STARTDATE STOPDATE", startDate, stopDate)
    var startingDate = moment(startDate, 'MM-DD-YYYY'); //StartDate "2001-01-20T07:00:00.000Z"
    var endingDate = moment(stopDate, 'YYYY-MM-DD'); //STOP DAte "2020-08-12T06:00:00.000Z"

    while (startingDate <= endingDate) {
      dateArray.push(moment(startingDate, 'YYYY-MM-DD'));
      startingDate = moment(startingDate, 'YYYY-MM-DD').add(1, unit);
    }

    return dateArray;
  };

  const compareArrays = (originalArr, dateArray) => {
    const result = dateArray.filter(e => !originalArr.some(o => o.displayDate === e.format('YYYY-MM-DD')));

    result.forEach(e => originalArr.push({ displayDate: e, time: null, ratingNumber: null }));
    originalArr.sort((a, b) => {
      let dateA = new Date(a.displayDate), dateB = new Date(b.displayDate);
      return dateA - dateB;
    });
    return originalArr;
  };

  function reformatDate(sizedArray) {
    let copy = [...sizedArray];
    copy.forEach(e => {
      e.displayDate = moment(e.displayDate, 'YYYY-MM-DD').format('MMM D');
    });
    return copy;
  }

  //Check if entry time is AM or PM for Daily Trend line
  function isAM(entry) {
    return moment(entry.time, 'hh:mm:ss').isBefore( moment('12:00:00', 'hh:mm:ss') )
  }

  //If arr is larger than 30 datapoints, reduce it to 30
  function reduceArrayLength(rateQuestion) {
    if (rateQuestion.length > 30) {
      return rateQuestion.slice(Math.max(rateQuestion.length - 30, 0));
    }
    return rateQuestion;
  }

  function removeOldEntries(array, rangeStartDate) {
    let i = 0
    while (i<array.length)
    {
        if (moment(array[i].date_created).isBefore(moment(rangeStartDate)))
        {
          array.splice(i,1)
        }
        else ++i
    }
    return array
  }

  function configureDateArray(sizedArray, arrLen, questionFrequency) {
    // let mostRecentDate = sizedArray[arrLen - 1].displayDate;
    let mostRecentDate = moment().format('YYYY-MM-DD');
    let oldestDate = moment(sizedArray[0].displayDate, 'YYYY-MM-DD')
    let rangeEndDate = moment(mostRecentDate, 'YYYY-MM-DD'); // 2020-09-09
    let timeToSubtract = 0;

    if (questionFrequency === 2) {
      timeToSubtract = moment(rangeEndDate, 'YYYY-MM-DD').diff(moment(oldestDate, 'YYYY-MM-DD'), 'weeks'); //rangeEndDate "2012-01-01T07:00:00.000Z"
    }

    if (questionFrequency === 1 || questionFrequency === 0) {
      timeToSubtract = moment(rangeEndDate, 'YYYY-MM-DD').diff(moment(oldestDate, 'YYYY-MM-DD'), 'days'); //rangeEndDate "2012-01-01T07:00:00.000Z"
    }

    // console.log('DTS', timeToSubtract);
    if (timeToSubtract > 30) {
      timeToSubtract = 30;
    }
    // console.log('DTS2', timeToSubtract);

    let rangeStartDate;
    if (questionFrequency === 2) {
      rangeStartDate = moment(mostRecentDate, 'YYYY-MM-DD').subtract(timeToSubtract, 'weeks');
    }
    if (questionFrequency === 1 || questionFrequency === 0) {
      rangeStartDate = moment(mostRecentDate, 'YYYY-MM-DD').subtract(timeToSubtract, 'days');
    }

    sizedArray = removeOldEntries(sizedArray, rangeStartDate)

    // console.log('arrLen', arrLen);
    // console.log('mostRecentDate', mostRecentDate);
    // console.log('rangeEndDate', rangeEndDate);
    // console.log('rangeStartDate', rangeStartDate);

    let dateArray = [];
    if (questionFrequency === 2) {
      dateArray = getDatesArr(rangeStartDate, rangeEndDate, "weeks");
    }
    if (questionFrequency === 1 || questionFrequency === 0) {
      dateArray = getDatesArr(rangeStartDate, rangeEndDate, "days");
    }
    // console.log("DATEARR", dateArray)
    // console.log("SIZEDARR", sizedArray)
    sizedArray = compareArrays(sizedArray, dateArray);
    return sizedArray
  }

  function handleDailyTrends(rateQuestion) {
    let sizedArray = rateQuestion.responses;

    let [AM, PM] = sizedArray.reduce(([AM, PM], entry) => {
      return isAM(entry) ? [[...AM, entry], PM] : [AM, [...PM, entry]]
    }, [[], []])

    reduceArrayLength(AM)
    reduceArrayLength(PM)
    let amLen = AM.length
    let pmLen = PM.length

    if (amLen > 1) {
      AM = configureDateArray(AM, amLen, 0)
    }
    if (pmLen > 1) {
      PM = configureDateArray(PM, pmLen, 0)
    }
    reformatDate(AM)
    reformatDate(PM)
    return [AM, PM]
  }

  const CreateTrendCharts = () => {
    return (
      answer.map((rateQuestion, i) => {
        let dataPoints = [];
        let dataPointsPM = [];
        let AMPM = [];
        let sizedArray = [];

        if (rateQuestion.responses === null || rateQuestion.responses.length === 0 || rateQuestion.responses === undefined) {
          console.log('NullorZero');
          return;
        }

      if (rateQuestion.frequency === 0) {
        AMPM = handleDailyTrends(rateQuestion)
      }

      if (rateQuestion.frequency === 1 || rateQuestion.frequency === 2 ) {
        sizedArray = reduceArrayLength(rateQuestion.responses)

        let arrLen = sizedArray.length;
        if (arrLen > 1) {
          sizedArray = configureDateArray(sizedArray, arrLen, rateQuestion.frequency)
        }

        reformatDate(sizedArray)

      }

        // console.log('clonedArray', clonedArray);

        return (
          rateQuestion.isChecked && rateQuestion.frequency === 0 ?
            <View key={i}>
              <VictoryChart domain={{ y: [1, 10] }}>
                <VictoryLabel
                  x={25}
                  y={24}
                  text={
                    rateQuestion.questionText.length > 60
                      ?
                      rateQuestion.questionText.substring(0, 50) + '...'
                      :
                      rateQuestion.questionText
                  }
                />
                {AMPM[0].map(p => {
                  dataPoints.push({ x: p.displayDate, y: p.ratingNumber });
                })}
                {AMPM[1].map(p => {
                  dataPointsPM.push({ x: p.displayDate, y: p.ratingNumber });
                })}

                <VictoryLine
                  data={dataPoints}
                  style={{ data: { color: "#b86942", stroke: "#b86942" } }}
                />
                <VictoryLine
                  data={dataPointsPM}
                  style={{ data: { color: "darkblue", stroke: "darkblue" } }}
                />
                {/*{console.log('DP', dataPoints)}*/}
                <VictoryAxis
                  dependentAxis={true}
                  style={{
                    ticks: { stroke: 'grey', size: 8 },
                    axisLabel: { fontSize: 33, padding: 20, angle: 0 },
                    grid: { stroke: 'grey' },
                  }}
                />

                <VictoryAxis
                  tickCount={8}
                  style={{
                    ticks: { stroke: 'grey', size: 10 },
                    tickLabels: { angle: 305, padding: 10 },
                  }}
                />

                <VictoryScatter
                  data={dataPoints}
                  size={3}
                  style={{ data: { fill: "#b86942", stroke: "#b86942" } }}

                />
                <VictoryScatter
                  data={dataPointsPM}
                  size={3}
                  style={{ data: { fill: "darkblue", stroke: "darkblue" } }}

                />
              </VictoryChart>

            </View>
            :
            <View key={i}>
              <VictoryChart domain={{ y: [1, 10] }}>
                <VictoryLabel
                  x={25}
                  y={24}
                  text={
                    rateQuestion.questionText.length > 60
                      ?
                      rateQuestion.questionText.substring(0, 50) + '...'
                      :
                      rateQuestion.questionText
                  }
                />
                {sizedArray.map(p => {
                  dataPoints.push({ x: p.displayDate, y: p.ratingNumber });
                })}

                <VictoryLine data={dataPoints}/>
                <VictoryAxis
                  dependentAxis={true}
                  style={{
                    data: { stroke: 'red' },
                    ticks: { stroke: 'grey', size: 8 },
                    axisLabel: { fontSize: 33, padding: 20, angle: 0 },
                    grid: { stroke: 'grey' },
                  }}
                />

                <VictoryAxis
                  tickCount={8}
                  style={{
                    ticks: { stroke: 'grey', size: 10 },
                    tickLabels: { angle: 305, padding: 10 },
                  }}
                />

                <VictoryScatter
                  data={dataPoints}
                  size={3}
                />
              </VictoryChart>
            </View>
        );
      })
    );
  };


  return (
    <>
      <Header title="Trends"/>
      {answer === undefined ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <ScrollView>
          <View style={styles.container}>
            <View style={{flexDirection: 'row'}}>
              <Txt style={{color: "#b86942"}}>AM Entries    </Txt>
              <Txt style={{color: "darkblue"}}>PM Entries</Txt>
            </View>

            <CreateTrendCharts/>

          </View>
        </ScrollView>
      }
    </>
  );
};


export default Trends;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(227,226,226,0.3)',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
