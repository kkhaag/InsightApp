import React, { useEffect, useState } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { CalendarList } from 'react-native-calendars';

import { Header } from '../otherViews/header';
import { Load } from '../../helpers/helperFunctions';

const CalendarView = ({ navigation }) => {
  const [state, setState] = useState(undefined);
  const [marked, setMarked] = useState(undefined);

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let data = await Load('journals');
      await organizeObj(data)
      await setState(data);
    });
  }, []);

  const journal = { key: 'journal', color: 'blue' };

  async function organizeObj(data) {
    let markedDatesObj = {};
    data.forEach(journalEntry => {
        markedDatesObj[journalEntry.displayDate] = { dots: [journal] };
      },
    );
    await setMarked(markedDatesObj)
  }

  return (
    <>
      <Header title="Calendar"/>
      {state === undefined || marked === undefined ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <View style={{ paddingTop: 10, flex: 1 }}>

          <CalendarList
            horizontal={true}
            pagingEnabled={true}
            hideDayNames={false}
            markingType={'multi-dot'}
            markedDates= {marked}

            onVisibleMonthsChange={(months) => {
              console.log('now these months are visible', months);
            }}
            // Max amount of months allowed to scroll to the past. Default = 50
            pastScrollRange={50}
            // Max amount of months allowed to scroll to the future. Default = 50
            futureScrollRange={50}
            // Enable or disable scrolling of calendar list
            scrollEnabled={true}
            // Enable or disable vertical scroll indicator. Default = false
            showScrollIndicator={true}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={day => {
              console.log('selected day', day);
            }}/>

        </View>
      }
    </>
  );
};

export default CalendarView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(227,226,226,0.3)',
  },

  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
