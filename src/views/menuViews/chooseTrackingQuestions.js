import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, View } from 'react-native';
import { Header } from '../otherViews/header';
import { Button, ButtonGroup, CheckBox } from 'react-native-elements';
import { Load, ShowMessage } from '../../helpers/helperFunctions';
import { uploadSurveys } from '../../helpers/apiDataManagement';


const ChooseTrackingQuestions = ({ navigation }) => {
  const [check, setCheck] = useState([]);
  const [originalData, setOriginalData] = useState([]);

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      // let mydelete = await Delete("trackingQs")
      let data = await Load('surveys');
      let filteredData = data.filter(question => question.isDoctorQuestion === false);
      await setCheck(filteredData);
    });
  }, []);

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let data = await Load('surveys');
      let filteredData = data.filter(question => question.isDoctorQuestion === false);
      await setOriginalData(filteredData);
    });
  }, [originalData]);

  const handleCheck = (question) => {
    check.forEach(trackingQ => {
      if (trackingQ._id === question._id) {
        trackingQ.isChecked = !question.isChecked;
      }
    });
    setCheck([...check]);
  };

  const handleReminderFrequency = (index, question) => {
    check.forEach(trackingQ => {
      if (trackingQ._id === question._id) {
        trackingQ.frequency = index;
      }
    });
    setCheck([...check]);
  };

  const SelectTrackingQuestions = () => {
    return (
      check.map((question, index) => {
          return (
            <View key={index}>
              <CheckBox
                title={question.questionText}
                checked={question.isChecked}
                onPress={() => handleCheck(question)}
                {...check}
              />

              {question.isChecked ?
                <ButtonGroup
                  buttons={['TWICE DAILY', 'DAILY', 'WEEKLY']}
                  selectedIndex={question.frequency}
                  onPress={(index) => handleReminderFrequency(index, question)}
                  containerStyle={{ marginBottom: 10 }}
                />
                : null}
            </View>
          );
        },
      ));
  };

  const HandleSave = async (storageKey) => {
    check.forEach(question => {
      if (question.isChecked === false) {
        question.responses = [];
        question.nextAnswerDate = '';
      }

      originalData.forEach(orig => {
        // If old and current questions are the same but frequency has changed, erase old data.
        if (orig._id === question._id && orig.frequency != question.frequency) {
          console.log("Freq", orig.frequency, question.frequency)
          question.responses = [];
          question.nextAnswerDate = '';
        }
      })
    });

    await setCheck([...check]);
    let questions = { questions: check };
    let status = await uploadSurveys(JSON.stringify(questions));
    if (!status) return;

    navigation.navigate('Home', { screen: 'Home' })
    await ShowMessage("success", "top", "Question Preferences Saved", 2000)

  };





  return (
    <>
      <Header title="Choose Questions"/>
      {check === undefined || check.length === 0 ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <ScrollView>
          <SelectTrackingQuestions/>
          <Button
            title="Save"
            style={{ marginTop: 50, width: 200, alignSelf: 'center' }}
            onPress={() => HandleSave('trackingQs')}
          />
        </ScrollView>
      }
    </>
  );
};

export default ChooseTrackingQuestions;




const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(227,226,226,0.3)',
  },

  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});
