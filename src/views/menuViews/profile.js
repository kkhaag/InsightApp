import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ScrollView, StyleSheet, Text, View } from 'react-native';

import { Header } from '../otherViews/header';
import { Avatar } from 'react-native-elements';
import { Load } from '../../helpers/helperFunctions';


const Profile = ({ navigation }) => {
  const [profile, setProfile] = useState();
  const [doctors, setDoctors] = useState();
  const [clinics, setClinics] = useState();

  useEffect(() => {
    let lambda = navigation.addListener('focus', async () => {
      let userData = await Load('user');
      let doctorData = await Load('doctors');
      let clinicData = await Load('clinics');
      setProfile(userData);
      setDoctors(doctorData);
      setClinics(clinicData);
    });
  }, []);

  return (
    <>
      <Header title="My Profile"/>
      {profile === undefined || doctors === undefined || clinics === undefined ?
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#0000ff"/>
        </View>
        :
        <ScrollView>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              marginTop: 0,
              backgroundColor: '#999999',
            }}>

            <Avatar
              size={90}
              icon={{ name: 'user', type: 'font-awesome' }}
              title={null}
            />
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-around',
              }}>
              <Text>Name: {profile.firstName} {profile.lastName}</Text>
              <Text>Date of Birth: {profile.dateOfBirth}</Text>
              <Text>Email Address: {profile.email}</Text>
            </View>
          </View>

          <Text style={[
            styles.titleText, { marginTop: 10, color: '#666666', margin: 10, textAlign: 'left' }]}>
            Link your Profile to a Health Professional Using their Unique ID:
          </Text>

          {/*<View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>*/}
          {/*  <View style={styles.textAreaContainer}>*/}
          {/*    <TextInput*/}
          {/*      style={styles.textArea}*/}
          {/*      underlineColorAndroid="transparent"*/}
          {/*      placeholder="Unique ID"*/}
          {/*      placeholderTextColor="grey"*/}
          {/*      multiline={false}*/}
          {/*    />*/}
          {/*  </View>*/}
          {/*  <Button*/}
          {/*    title="Link"*/}
          {/*    containerStyle={{ height: 40, width: 150, margin: 10, marginTop: 18 }}*/}
          {/*    titleStyle={{ color: 'white', marginHorizontal: 20 }}*/}
          {/*    onPress={() => handlePress()}*/}
          {/*  />*/}
          {/*</View>*/}

          <Text style={{ marginTop: 15, color: '#666666', margin: 10, textAlign: 'left', fontSize: 24 }}>
            Associated Health Professionals:
          </Text>
          <View key={doctors._id}>
            <Text style={styles.titleText}> {doctors.first_name} {doctors.last_name} </Text>
            <Text style={styles.subTitleText}> Contact number: {doctors.personal_phone}</Text>
            <Text style={styles.subTitleText}> Contact email: {doctors.email}</Text>
            <Text style={styles.subTitleText}> Address: {clinics.address}</Text>
          </View>
        </ScrollView>
      }
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(227,226,226,0.3)',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 5,
    paddingTop: 10,
    fontFamily: Platform.OS === 'ios' ? 'Arial' : null,
  },
  subTitleText: {
    fontSize: 16,
    marginLeft: 20,
    fontFamily: Platform.OS === 'ios' ? 'Arial' : null,
  },
  textAreaContainer: {
    margin: 10,
    height: 55,
    borderColor: 'grey',
    borderWidth: 3,
    padding: 10,
    width: 150,
  },
  textArea: {
    justifyContent: 'flex-start',
    height: 30,
    fontSize: 18,
  },
});

export default Profile;
