import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Dimensions,
  ImageBackground,
  LayoutAnimation,
  Platform,
  StyleSheet,
  Text,
  UIManager,
  View,
  TextInput, KeyboardAvoidingView, Keyboard,
} from 'react-native';
import { Button } from 'react-native-elements';
// import { TouchableItem } from '@react-navigation/stack/src/views/TouchableItem';
// import DateTimePickerModal from 'react-native-modal-datetime-picker';
import SignUp from './signUp';
import { fetchAllData, getUserAndPassword, uploadPasswordOrExpoToken } from '../../helpers/apiDataManagement';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import {
  Delete,
  getLocalAuthToken,
  isSignedIn,
  Load,
  Save,
  ShowMessage,
} from '../../helpers/helperFunctions';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as Notifications from 'expo-notifications';
import { FontAwesome, FontAwesome5, SimpleLineIcons } from '@expo/vector-icons';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const BG_IMAGE = require('../../../assets/images/bg_screen2.jpg');

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
UIManager.setLayoutAnimationEnabledExperimental(true);
const TabSelector = ({ selected }) => {
  {
    // console.log('selected', selected);
  }
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected}/>
    </View>
  );
};

TabSelector.propTypes = {
  selected: PropTypes.bool.isRequired,
};


const LoginScreen = ({ navigation }) => {

  useEffect(() => {
    let lambda = useNav.addListener('focus', async () => {
    let email = await Load("email")
      setState({ ...state, email: email, password: '', passwordConfirmation: '' })
    });
  }, [] )

  const useNav = useNavigation();

  useEffect(() => {

    // Delete("email")
    async function loginStatus() {
      let emailFoundForSignIn = await isSignedIn();
      console.log("emailFoundForSignIn", emailFoundForSignIn)

      if (emailFoundForSignIn) {
        let token = await getLocalAuthToken()
        console.log("getLocalAuthToken", token)

        // If everything checks out, get push token sent to DB
        if (emailFoundForSignIn && token.success) {
          console.log("getting ready to push push token to DB")
          await registerForPushNotificationsAsync()
          useNav.navigate('Home', { screen: 'Home' })
        }
      }

    }
    loginStatus();
  }, []);

  const [expoPushToken, setExpoPushToken] = useState('');

  async function registerForPushNotificationsAsync() {
    let token;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync()
      let finalStatus = existingStatus;

      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      let dataToUpdate = { 'expo_push_token': token };
      console.log("Push Token before upload", token)

      let email = await Load("email")
      console.log("email2:", email)
      await uploadPasswordOrExpoToken(dataToUpdate, email);
    }
    else {
      // alert('Must use physical device for Push Notifications');
      await ShowMessage("danger", "top", "Must use physical device for Push Notifications")
    }

    if (Platform.OS === 'android') {
      await Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
    // return token;
  }


  const [state, setState] = useState({
    email: '',
    password: '',
    passwordConfirmation: '',
    birthdate: '',
    selectedCategory: 0,
    isLoading: false,
    isEmailValid: true,
    isPasswordValid: true,
    isConfirmationValid: true,
  });

  // const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  //
  // const showDatePicker = () => {
  //   setDatePickerVisibility(true);
  // };
  //
  // const hideDatePicker = () => {
  //   setDatePickerVisibility(false);
  // };

  // const handleConfirm = (date) => {
  //   let month = date.getMonth() + 1; //months from 1-12
  //   let day = date.getDate();
  //   let year = date.getFullYear();
  //   let dob = `${month}/${day}/${year}`;
  //   setState({ ...state, birthdate: dob });
  //   hideDatePicker();
  // };


  const selectCategory = (selectedCategory) => {
    LayoutAnimation.easeInEaseOut();
    setState({
      ...state,
      selectedCategory,
      isLoading: false,
    });
  };

  const validateEmail = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  async function validation() {
    setTimeout(() => {
      LayoutAnimation.easeInEaseOut();
      setState({
        ...state,
        isLoading: false,
        isEmailValid: validateEmail(email),
        isPasswordValid: password.length >= 8,
        isConfirmationValid:
          password === passwordConfirmation,
      });
    }, 1500);
  }

  const login = async () => {
    const { email, password, birthdate, passwordConfirmation } = state;
    console.log("EMAMIL", email)
    setState({ ...state, isLoading: true });

    // Validate is viable email, password length, and matching passwords
    await validation();
    if (!validateEmail(email) || password.length < 8 || password !== passwordConfirmation) return;

    // Get stored password from server using supplied email
    let userP = await getUserAndPassword(email);

    // If stored password is null, empty, or undefined, update create new password for email.
    if (userP === '' || userP === null || userP === undefined) {
      await uploadPasswordOrExpoToken({ password: password, date_of_birth: birthdate }, email);
    }

    // If stored password and current password do not match, error
    if (userP !== password) {
      alert('There was an issue with the request, please check your credentials and try again');
      return;
    }
    Keyboard.dismiss()
    await new Promise(r => setTimeout(r, 500));

    await Save(email, 'email');
    useNav.navigate('Home', { screen: 'Home' });
    await registerForPushNotificationsAsync()
  };


  const {
    selectedCategory,
    isLoading,
    isEmailValid,
    isPasswordValid,
    passwordConfirmation,
    isConfirmationValid,
    email,
    password,
  } = state;

  const isLoginPage = selectedCategory === 0;
  const isSignUpPage = selectedCategory === 1;
  let emailInput = useRef();
  let passwordInput = useRef();
  let confirmationInput = useRef();
  let birthdateInput = useRef();

  return (
    <View style={styles.container}>
      {}
      <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
        <KeyboardAvoidingView
          contentContainerStyle={styles.loginContainer}
        >

        <View style={styles.titleContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.titleText}>INSIGHT</Text>
          </View>
          <View style={{ marginTop: -5, marginLeft: 10 }}>
            <Text style={styles.titleText}>The Whole Picture</Text>
          </View>
        </View>


        {/*<View style={{ flexDirection: 'row' }}>*/}

        {/*  <Button*/}
        {/*    disabled={isLoading}*/}
        {/*    type="clear"*/}
        {/*    activeOpacity={0.7}*/}
        {/*    onPress={() => selectCategory(0)}*/}
        {/*    containerStyle={{ flex: 1 }}*/}
        {/*    titleStyle={[*/}
        {/*      styles.categoryText,*/}
        {/*      isLoginPage && styles.selectedCategoryText,*/}
        {/*    ]}*/}
        {/*    title={'Login'}*/}
        {/*  />*/}

        {/*  <Button*/}
        {/*    disabled={isLoading}*/}
        {/*    type="clear"*/}
        {/*    activeOpacity={0.7}*/}
        {/*    onPress={() => selectCategory(1)}*/}
        {/*    containerStyle={{ flex: 1 }}*/}
        {/*    titleStyle={[*/}
        {/*      styles.categoryText,*/}
        {/*      isSignUpPage && styles.selectedCategoryText,*/}
        {/*    ]}*/}
        {/*    title={'Sign up'}*/}
        {/*  />*/}
        {/*</View>*/}


        {/*<View style={styles.rowSelector}>*/}
        {/*  <TabSelector selected={isLoginPage} />*/}
        {/*  <TabSelector selected={isSignUpPage} />*/}
        {/*</View>*/}

        {state.selectedCategory === 0 ? (

          <View style={styles.formContainer}>


            {/*EMAIL INPUT*/}
            <View style={[styles.inputStyle1, isEmailValid ? styles.textValid : styles.textInvalid]}>
              <View style={{alignContent: 'center', alignSelf: 'center' }}>
                <FontAwesome
                  name="envelope-o"
                  size={25}
                  color="rgba(0, 0, 0, 0.38)"
                  style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                />
              </View>
              <View style={{ display: 'flex', flex: 1}}>
              <TextInput
                textContentType='oneTimeCode'
                placeholderTextColor="rgba(0, 0, 0, 0.5)"
                placeholder={'Email'}
                blurOnSubmit={false}
                value={email}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardAppearance="light"
                keyboardType="email-address"
                style={{ marginLeft: 14, fontSize: 18, minHeight: 40}}
                ref={emailInput}
                onSubmitEditing={() => passwordInput.current.focus()}
                onChangeText={(email) => setState({ ...state, email })}
                autoFocus={true}
                // onFocus={() => alert("Email was focused")}
               />
              </View>
            </View>
            {isEmailValid ? null : <Text style={styles.alertText}>Please enter a valid email address</Text>}



            {/*PASSWORD INPUT*/}
            <View style={[styles.inputStyle2, isPasswordValid ? styles.textValid : styles.textInvalid]}>
              <View style={{alignContent: 'center', alignSelf: 'center' }}>
                <SimpleLineIcons
                  name="lock"
                  size={25}
                  color="rgba(0, 0, 0, 0.38)"
                  style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                />
              </View>

              <View style={{ display: 'flex', flex: 1}}>
                <TextInput
                  textContentType='oneTimeCode'
                  placeholderTextColor="rgba(0, 0, 0, 0.5)"
                  placeholder={'Password'}
                  value={password}
                  blurOnSubmit={false}
                  secureTextEntry={true}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardAppearance="light"
                  // keyboardType="email-address"
                  style={{ marginLeft: 14, fontSize: 18, minHeight: 40}}
                  ref={passwordInput}
                  onSubmitEditing={() => confirmationInput.current.focus()}
                  onChangeText={(password) => setState({ ...state, password })}
                />
              </View>
            </View>

            {isPasswordValid ? null : <Text style={styles.alertText}>Please enter at least 8 characters</Text>}


            {/*CONFIRM PASSWORD INPUT*/}
            <View style={[styles.inputStyle2, isConfirmationValid ? styles.textValid : styles.textInvalid]}>
              <View style={{alignContent: 'center', alignSelf: 'center' }}>
                <SimpleLineIcons
                  name="lock"
                  size={25}
                  color="rgba(0, 0, 0, 0.38)"
                  style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                />
              </View>

              <View style={{ display: 'flex', flex: 1}}>
                <TextInput
                  textContentType='oneTimeCode'
                  placeholderTextColor="rgba(0, 0, 0, 0.5)"
                  placeholder={'Confirm Password'}
                  value={passwordConfirmation}
                  secureTextEntry={true}
                  autoCapitalize="none"
                  autoCorrect={false}
                  returnKeyType={'done'}
                  blurOnSubmit={false}
                  keyboardAppearance="light"
                  // keyboardType="email-address"
                  style={{ marginLeft: 14, fontSize: 18, minHeight: 40}}
                  ref={confirmationInput}
                  // onSubmitEditing={() => showDatePicker()} //Used when datepicker
                  onChangeText={(passwordConfirmation) => setState({ ...state, passwordConfirmation })}
                  onSubmitEditing={() => login()}
                />
              </View>
            </View>
            {isConfirmationValid ? null : <Text style={styles.alertText}>Please enter the same password</Text>}




<View style={{alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.loginButton}
              containerStyle={{ marginTop: 32, flex: 0 }}
              activeOpacity={0.8}
              title={'LOGIN'}
              onPress={() => login()}
              titleStyle={styles.loginTextButton}
              loading={isLoading}
              disabled={isLoading}
            />
</View>

          </View>
        ) : <SignUp/>}
        </KeyboardAvoidingView>
      </ImageBackground>
    </View>
  );

};

export default LoginScreen;


const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },

  alertText: {
    color: 'red',
    marginLeft: 24,
    marginTop: 5
  },
  inputStyle1: {
    display: 'flex',
    flexDirection: 'row',
    borderBottomWidth: 1,
  },
    textValid: {
      borderBottomColor: "grey",
    },
    textInvalid: {
      borderBottomColor: 'red',
    },

  inputStyle2: {
    display: 'flex',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: "grey",
    marginTop: 20

  },

  rowSelector: {
    height: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  selectorContainer: {
    flex: 1,
    alignItems: 'center',
  },
  selected: {
    position: 'absolute',
    borderRadius: 50,
    height: 0,
    width: 0,
    top: -5,
    borderRightWidth: 70,
    borderBottomWidth: 70,
    borderColor: 'white',
    backgroundColor: 'white',
  },
  loginContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginTextButton: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold',
  },
  loginButton: {
    borderRadius: 10,
    height: 50,
    width: 200,
  },
  titleContainer: {
    height: 100,
    backgroundColor: 'transparent',
    justifyContent: 'center',
  },
  formContainer: {
    display: 'flex',
    backgroundColor: 'white',
    width: SCREEN_WIDTH - 30,
    borderRadius: 10,
    paddingTop: 32,
    paddingBottom: 32,
    // alignItems: 'center',
  },
  formContainer2: {
    backgroundColor: 'white',
    width: SCREEN_WIDTH - 30,
    borderRadius: 10,
    paddingTop: 0,
    paddingBottom: 0,
    alignItems: 'center',
    color: 'black',
    opacity: 1,
  },
  loginText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
    fontFamily: 'light',
    backgroundColor: 'transparent',
    opacity: 0.54,
  },
  selectedCategoryText: {
    opacity: 1,
  },
  titleText: {
    color: 'white',
    fontSize: 30,
    fontFamily: 'regular',
  },

});



{/*EMAIL INPUT*/}
{/* <Input
      leftIcon={
        <Icon
          name="envelope-o"
          type="font-awesome"
          color="rgba(0, 0, 0, 0.38)"
          size={25}
          style={{ backgroundColor: 'transparent' }}
        />
      }
      value={email}
      keyboardAppearance="light"
      autoFocus={false}
      autoCapitalize="none"
      autoCorrect={false}
      keyboardType="email-address"
      returnKeyType="next"
      inputStyle={{ marginLeft: 10 }}
      placeholder={'Email'}
      containerStyle={{
        borderBottomColor: 'rgba(0, 0, 0, 0.38)',
      }}
      ref={emailInput}
      onSubmitEditing={() => passwordInput.current.focus()}
      onChangeText={(email) => setState({ ...state, email })}
      errorMessage={
        isEmailValid ? null : 'Please enter a valid email address'
      }
    />*/}



{/*PASSWORD INPUT*/}
{/*<Input*/}
{/*  leftIcon={*/}
{/*    <Icon*/}
{/*      name="lock"*/}
{/*      type="simple-line-icon"*/}
{/*      color="rgba(0, 0, 0, 0.38)"*/}
{/*      size={25}*/}
{/*      style={{ backgroundColor: 'transparent' }}*/}
{/*    />*/}
{/*  }*/}
{/*  value={password}*/}
{/*  keyboardAppearance="light"*/}
{/*  autoCapitalize="none"*/}
{/*  autoCorrect={false}*/}
{/*  secureTextEntry={true}*/}
{/*  // returnKeyType={isSignUpPage ? 'next' : 'done'}*/}
{/*  blurOnSubmit={true}*/}
{/*  containerStyle={{*/}
{/*    marginTop: 16,*/}
{/*    borderBottomColor: 'rgba(0, 0, 0, 0.38)',*/}
{/*  }}*/}
{/*  inputStyle={{ marginLeft: 10 }}*/}
{/*  placeholder={'Password'}*/}
{/*  ref={passwordInput}*/}
{/*  onSubmitEditing={() => confirmationInput.current.focus()}*/}

{/*  onChangeText={(password) => setState({ ...state, password })}*/}
{/*  errorMessage={ isEmailValid ? null : showMessage({*/}
{/*    message: 'The password must be at least 8 characters',*/}
{/*    type: 'danger',*/}
{/*    position: 'top'*/}
{/*  })*/}
{/*  }*/}
{/*/>*/}


{/*CONFIRM PASSWORD INPUT*/}
// <Input
//   leftIcon={
//     <Icon
//       name="lock"
//       type="simple-line-icon"
//       color="rgba(0, 0, 0, 0.38)"
//       size={25}
//       style={{ backgroundColor: 'transparent' }}
//     />
//   }
//   value={passwordConfirmation}
//   secureTextEntry={true}
//   keyboardAppearance="light"
//   autoCapitalize="none"
//   autoCorrect={false}
//   returnKeyType={'done'}
//   blurOnSubmit={true}
//   containerStyle={{
//     marginTop: 16,
//     borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//   }}
//   inputStyle={{ marginLeft: 10 }}
//   placeholder={'Confirm Password'}
//   ref={confirmationInput}
//   onChangeText={(passwordConfirmation) => setState({ ...state, passwordConfirmation })}
//   // onSubmitEditing={() => showDatePicker()} //Used when datepicker
//   onSubmitEditing={() => login()}
//   errorMessage={ isEmailValid ? null : showMessage({
//     message: 'Please enter matching passwords',
//     type: 'danger',
//     position: 'top'
//   })
//   }
// />

{/*DATE OF BIRTH INPUT*/}
{/*<TouchableItem style={styles.formContainer2} onPress={() => showDatePicker()}>*/}
{/*  <Input*/}
{/*    leftIcon={*/}
{/*      <Icon*/}
{/*        name="calendar"*/}
{/*        type="simple-line-icon"*/}
{/*        color="rgba(0, 0, 0, 0.38)"*/}
{/*        size={25}*/}
{/*        style={{ backgroundColor: 'transparent' }}*/}
{/*      />*/}
{/*    }*/}
{/*    disabled={true}*/}
{/*    disabledInputStyle={{ opacity: 1 }}*/}
{/*    blurOnSubmit={true}*/}
{/*    containerStyle={{*/}
{/*      marginTop: 16,*/}
{/*      borderBottomColor: 'rgba(0, 0, 0, 0.38)',*/}
{/*    }}*/}
{/*    inputStyle={{ marginLeft: 10 }}*/}
{/*    placeholder={'Date of Birth'}*/}
{/*    onSubmitEditing={() => login()}*/}
{/*    onEndEditing={() => login()}*/}
{/*    onSubmitEditing={() => login()}*/}

{/*    ref={birthdateInput}*/}
{/*    value={state.birthdate}*/}

{/*  />*/}
{/*</TouchableItem>*/}

{/*<DateTimePickerModal*/}
{/*  isVisible={isDatePickerVisible}*/}
{/*  mode="date"*/}
{/*  onConfirm={handleConfirm}*/}
{/*  onCancel={hideDatePicker}*/}
{/*  date={new Date()}*/}
{/*/>*/}
