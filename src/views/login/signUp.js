// import React, { useRef, useState } from 'react';
// import { Dimensions, KeyboardAvoidingView, LayoutAnimation, StyleSheet, UIManager, View } from 'react-native';
// import { Button, Icon, Input } from 'react-native-elements';
// import { TouchableItem } from '@react-navigation/stack/src/views/TouchableItem';
// import DateTimePickerModal from 'react-native-modal-datetime-picker';
//
//
// const SCREEN_WIDTH = Dimensions.get('window').width;
// const SCREEN_HEIGHT = Dimensions.get('window').height;
//
// // Enable LayoutAnimation on Android
// UIManager.setLayoutAnimationEnabledExperimental &&
// UIManager.setLayoutAnimationEnabledExperimental(true);
//
//
// const SignUp = () => {
//
//   const [state, setState] = useState({
//     name: '',
//     email: '',
//     password: '',
//     birthdate: '',
//     healthProfessionalId: '',
//     selectedCategory: 0,
//     isLoading: false,
//     isEmailValid: true,
//     isPasswordValid: true,
//     isConfirmationValid: true,
//   });
//
//   const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
//
//   const showDatePicker = () => {
//     // console.log('ShowDateP');
//     setDatePickerVisibility(true);
//   };
//
//   const hideDatePicker = () => {
//     setDatePickerVisibility(false);
//   };
//
//   const handleConfirm = (date) => {
//     let month = date.getMonth() + 1; //months from 1-12
//     let day = date.getDate();
//     let year = date.getFullYear();
//     let dob = `${month}/${day}/${year}`;
//     setState({ ...state, birthdate: dob });
//     hideDatePicker();
//     // healthProfessionalInput.current.focus()
//   };
//
//
//   const validateEmail = (email) => {
//     var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return re.test(email);
//   };
//
//
//   const signUp = () => {
//     const { email, password, passwordConfirmation, birthdate } = state;
//     setState({ ...state, isLoading: true });
//     // Simulate an API call
//     setTimeout(() => {
//       LayoutAnimation.easeInEaseOut();
//       setState({
//         ...state,
//         isLoading: false,
//         isEmailValid: validateEmail(email) || emailInput.current.shake(),
//         isPasswordValid: password.length >= 8 || passwordInput.current.shake(),
//         isConfirmationValid:
//           password === passwordConfirmation || confirmationInput.current.shake(),
//       });
//     }, 1500);
//   };
//
//
//   const {
//     isLoading,
//     isEmailValid,
//     isPasswordValid,
//     isConfirmationValid,
//     email,
//     password,
//     passwordConfirmation,
//     birthdate,
//     name,
//     healthProfessionalId,
//   } = state;
//
//
//   let emailInput = useRef();
//   let passwordInput = useRef();
//   let confirmationInput = useRef();
//   let birthdateInput = useRef();
//   let healthProfessionalInput = useRef();
//
//   return (
//     <View style={styles.container}>
//       <KeyboardAvoidingView
//         contentContainerStyle={styles.loginContainer}
//         behavior="padding"
//       >
//
//         <View style={styles.formContainer}>
//
//           {/*NAME INPUT*/}
//           <Input
//             leftIcon={
//               <Icon
//                 name="user"
//                 type="simple-line-icon"
//                 color="rgba(0, 0, 0, 0.38)"
//                 size={25}
//                 style={{ backgroundColor: 'transparent' }}
//               />
//             }
//             value={name}
//             keyboardAppearance="light"
//             autoCapitalize="words"
//             autoCorrect={false}
//             keyboardType="default"
//             returnKeyType={'next'}
//             blurOnSubmit={true}
//             containerStyle={{
//               marginTop: 16,
//               borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//             }}
//             inputStyle={{ marginLeft: 10 }}
//             placeholder={'Name'}
//             onSubmitEditing={() => emailInput.current.focus()}
//             onChangeText={(name) => setState({ ...state, name })}
//           />
//
//           {/*EMAIL INPUT*/}
//           <Input
//             leftIcon={
//               <Icon
//                 name="envelope-o"
//                 type="font-awesome"
//                 color="rgba(0, 0, 0, 0.38)"
//                 size={25}
//                 style={{ backgroundColor: 'transparent' }}
//               />
//             }
//             value={email}
//             keyboardAppearance="light"
//             autoFocus={false}
//             autoCapitalize="none"
//             autoCorrect={false}
//             keyboardType="email-address"
//             returnKeyType="next"
//             inputStyle={{ marginLeft: 10 }}
//             placeholder={'Email'}
//             containerStyle={{
//               borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//             }}
//             ref={emailInput}
//             onSubmitEditing={() => passwordInput.current.focus()}
//             onChangeText={(email) => setState({ ...state, email })}
//             errorMessage={
//               isEmailValid ? null : 'Please enter a valid email address'
//             }
//           />
//
//           {/*PASSWORD INPUT*/}
//           <Input
//             leftIcon={
//               <Icon
//                 name="lock"
//                 type="simple-line-icon"
//                 color="rgba(0, 0, 0, 0.38)"
//                 size={25}
//                 style={{ backgroundColor: 'transparent' }}
//               />
//             }
//             value={password}
//             keyboardAppearance="light"
//             autoCapitalize="none"
//             autoCorrect={false}
//             secureTextEntry={true}
//             // returnKeyType={isSignUpPage ? 'next' : 'done'}
//             blurOnSubmit={true}
//             containerStyle={{
//               borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//             }}
//             inputStyle={{ marginLeft: 10 }}
//             placeholder={'Password'}
//             ref={passwordInput}
//             onSubmitEditing={() => confirmationInput.current.focus()}
//             onChangeText={(password) => setState({ ...state, password })}
//             errorMessage={
//               isPasswordValid
//                 ? null
//                 : 'Please enter at least 8 characters'
//             }
//           />
//
//           {/*CONFIRM PASSWORD INPUT*/}
//           <Input
//             leftIcon={
//               <Icon
//                 name="lock"
//                 type="simple-line-icon"
//                 color="rgba(0, 0, 0, 0.38)"
//                 size={25}
//                 style={{ backgroundColor: 'transparent' }}
//               />
//             }
//             value={passwordConfirmation}
//             secureTextEntry={true}
//             keyboardAppearance="light"
//             autoCapitalize="none"
//             autoCorrect={false}
//             keyboardType="default"
//             returnKeyType={'done'}
//             blurOnSubmit={true}
//             containerStyle={{
//               borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//             }}
//             inputStyle={{ marginLeft: 10 }}
//             placeholder={'Confirm Password'}
//             ref={confirmationInput}
//             onChangeText={(passwordConfirmation) =>
//               setState({ ...state, passwordConfirmation })
//             }
//
//             onSubmitEditing={() => showDatePicker()}
//
//             errorMessage={
//               isConfirmationValid
//                 ? null
//                 : 'Please enter the same password'
//             }
//           />
//
//           {/*BIRTHDATE INPUT*/}
//           <TouchableItem style={styles.formContainer2} onPress={() => showDatePicker()}>
//             <Input
//               leftIcon={
//                 <Icon
//                   name="calendar"
//                   type="simple-line-icon"
//                   color="rgba(0, 0, 0, 0.38)"
//                   size={25}
//                   style={{ backgroundColor: 'transparent' }}
//                 />
//               }
//               disabled={true}
//               disabledInputStyle={{ opacity: 1 }}
//               blurOnSubmit={true}
//               containerStyle={{
//                 borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//               }}
//               inputStyle={{ marginLeft: 10 }}
//               placeholder={'Date of Birth'}
//               ref={birthdateInput}
//
//               //   onSubmitEditing={() =>
//               //     healthProfessionalInput.current.focus()
//               // }
//               onTextInput={() =>
//                 healthProfessionalInput.current.focus()
//               }
//               //
//               // onChange={() => healthProfessionalInput.current.focus()}
//               // // onSubmitEditing={() => healthProfessionalInput.current.focus()}
//               value={state.birthdate}
//
//             />
//           </TouchableItem>
//
//           <DateTimePickerModal
//             isVisible={isDatePickerVisible}
//             mode="date"
//             onConfirm={handleConfirm}
//             onCancel={hideDatePicker}
//             date={new Date()}
//           />
//
//
//           {/*HEALTH PROFESSIONAL INPUT*/}
//           <Input
//             icon={
//               <Icon
//                 name="lock"
//                 type="simple-line-icon"
//                 color="rgba(0, 0, 0, 0.38)"
//                 size={25}
//                 style={{ backgroundColor: 'transparent' }}
//               />
//             }
//             value={healthProfessionalId}
//             keyboardAppearance="light"
//             autoCapitalize="none"
//             autoCorrect={false}
//             keyboardType="default"
//             returnKeyType={'done'}
//             blurOnSubmit={true}
//             containerStyle={{
//               borderBottomColor: 'rgba(0, 0, 0, 0.38)',
//             }}
//             ref={healthProfessionalInput}
//
//             inputStyle={{ marginLeft: 10 }}
//             placeholder={'Health Professional ID (Optional)'}
//             onSubmitEditing={signUp}
//             onChangeText={(healthProfessionalId) =>
//               setState({ ...state, healthProfessionalId })
//             }
//           />
//
//           <Button
//             buttonStyle={styles.loginButton}
//             containerStyle={{ marginTop: 0, flex: 0 }}
//             activeOpacity={0.8}
//             title={'SIGN UP'}
//             onPress={signUp}
//             titleStyle={styles.loginTextButton}
//             loading={isLoading}
//             disabled={isLoading}
//           />
//
//         </View>
//       </KeyboardAvoidingView>
//     </View>
//   );
//
// };
//
// export default SignUp;
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   loginContainer: {
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   loginTextButton: {
//     fontSize: 16,
//     color: 'white',
//     fontWeight: 'bold',
//   },
//   loginButton: {
//     borderRadius: 10,
//     height: 50,
//     width: 200,
//   },
//   formContainer: {
//     backgroundColor: 'white',
//     width: SCREEN_WIDTH - 30,
//     borderRadius: 10,
//     paddingTop: 0,
//     paddingBottom: 20,
//     alignItems: 'center',
//   },
//   formContainer2: {
//     backgroundColor: 'white',
//     width: SCREEN_WIDTH - 30,
//     borderRadius: 10,
//     paddingTop: 0,
//     paddingBottom: 0,
//     alignItems: 'center',
//     color: 'black',
//   },
//   loginText: {
//     fontSize: 16,
//     fontWeight: 'bold',
//     color: 'white',
//   },
// });
//
//
