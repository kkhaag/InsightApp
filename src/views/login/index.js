import React, { Component } from 'react';
import { Platform, ScrollView, StyleSheet, View } from 'react-native';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { Header } from '../otherViews/header';
import LoginScreen3 from './login';

export default class Login extends Component {
  render() {
    return (
      <>
        {/*<Header title="Login Example"/>*/}
        <View style={styles.container}>
          {Platform.OS === 'ios' ? <KeyboardAwareScrollView>
            <LoginScreen3/>
          </KeyboardAwareScrollView>
            :
          <ScrollView>
            <LoginScreen3/>
          </ScrollView>
          }
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
});
